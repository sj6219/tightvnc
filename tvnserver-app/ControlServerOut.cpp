// Copyright (C) 2009,2010,2011,2012 GlavSoft LLC.
// All rights reserved.
//
//-------------------------------------------------------------------------
// This file is part of the TightVNC software.  Please visit our Web site:
//
//                       http://www.tightvnc.com/
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//-------------------------------------------------------------------------
//

#include "ControlServerOut.h"
#include "ControlClient.h"
#include "tvncontrol-app/NamedPipeTransport.h"
#include "io-lib/ByteArrayOutputStream.h"
#include "rfb/MsgDefs.h"



//std::mutex ControlServerOut::Mutex;
//std::vector<NamedPipe *> ControlServerOut::PipeTable;
ControlServerOut* ControlServerOut::Control;

ControlServerOut::ControlServerOut(PipeServer *pipeServer,
                             RfbClientManager *rfbClientManager,
                             LogWriter *log)
: ControlServer(pipeServer, rfbClientManager, log)
{
  _ASSERT(ControlServerOut::Control == 0);
  m_log->message(_T("%s"), _T("ControlEx server started"));

  ControlServerOut::Control = this;
}

ControlServerOut::~ControlServerOut()
{
  ControlServerOut::Control = 0;

  m_log->message(_T("%s"), _T("ControlEx server stopped"));
}

void ControlServerOut::execute()
{
  try {
    while (!isTerminating()) {
      NamedPipe *pipe = m_pipeServer->accept();
      {
        //std::unique_lock<std::mutex> lk(Mutex);
        //for (auto handle : PipeTable) {
        //  delete handle;
        //}
        //PipeTable.clear();
        //PipeTable.push_back(pipe);
        m_threadCollector.destroyAllThreads();


        DWORD written = pipe->write("", 1);
        _ASSERT(written == 1);
      }


      Transport *transport = new NamedPipeTransport(pipe);

      ControlClient*clientThread = new ControlClient(transport,
                                                      m_rfbClientManager,
                                                      &m_authenticator,
                                                      pipe->getHandle(),
                                                      m_log);

      clientThread->resume();

      m_threadCollector.addThread(clientThread);
    }
  } catch (Exception &ex) {
    m_log->error(_T("Exception on ControlEx server thread: %s"), ex.getMessage());
  }
}

void ControlServerOut::WriteToPipe(const void* buffer, size_t len)
{
#if defined(_DEBUG) && 0
  static int count;
  _RPT(L"WriteToPipe %d %d %d\n", ++count, len, ((char *)buffer)[0]);
  ByteArrayOutputStream bufferStream;
  DataOutputStream stream(&bufferStream);
  stream.writeUInt32(0x12345678);
  stream.writeUInt32(len);
  stream.writeFully(buffer, len);
  stream.flush();
  buffer = bufferStream.toByteArray();
  len = bufferStream.size();
#endif

    ControlServerOut::Control->m_threadCollector.for_each(
      [=](Thread* thread) {
        ControlClient* client = (ControlClient*)thread;
        client->write(buffer, len);
      });

  //std::unique_lock<std::mutex> lk(Mutex);
  //for (auto it = PipeTable.begin(); it != PipeTable.end(); ) {
  //  NamedPipe* pipe = *it;
  //  try {
  //    DWORD  written = pipe->write(buffer, len);
  //    _ASSERT(written == len);
  //  }
  //  catch (Exception e) {
  //    delete pipe;
  //    it = PipeTable.erase(it);
  //    continue;
  //  }
  //  ++it;
  //}
}

void ControlServerOut::SendStart(LPCTSTR host)
{
  char buffer[256];
  ByteArrayOutputStream bufferStream(buffer);
  DataOutputStream stream(&bufferStream);

  stream.writeUInt8(ClientMsgDefs::CONNECTION_START);
  stream.writeUTF8(host);
  stream.flush();

  ControlServerOut::WriteToPipe(bufferStream.toByteArray(), bufferStream.size());
}

void ControlServerOut::SendStop(LPCTSTR host)
{
  char buffer[256];
  ByteArrayOutputStream bufferStream(buffer);
  DataOutputStream stream(&bufferStream);

  stream.writeUInt8(ClientMsgDefs::CONNECTION_STOP);
  stream.writeUTF8(host);
  stream.flush();

  ControlServerOut::WriteToPipe(bufferStream.toByteArray(), bufferStream.size());
}

