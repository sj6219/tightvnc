// Copyright (C) 2010,2011,2012 GlavSoft LLC.
// All rights reserved.
//
//-------------------------------------------------------------------------
// This file is part of the TightVNC software.  Please visit our Web site:
//
//                       http://www.tightvnc.com/
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//-------------------------------------------------------------------------
//

#include "Clipboard.h"
#include <shlobj_core.h>
#include "UnicodeStringStorage.h"
#include "AnsiStringStorage.h"
#include "Utf8StringStorage.h"
#include <mutex>
#include <string>

static int Port;
static std::mutex Mutex;
static StringStorage ClipText;

StringStorage Clipboard::addCR(const StringStorage* str)
{
  const TCHAR* beginString = str->getString();
  const TCHAR* endString = beginString + str->getLength() + 1; // start + lenght + '\0'
  vector<TCHAR> chars(beginString, endString);
  vector<TCHAR> newChars(str->getLength() * 2 + 1);
  size_t countLF = 0;
  for (size_t i = 0; i < chars.size(); i++) {
    // if is first byte or previous byte not CR, then add CR
    if ((i == 0 || chars[i - 1] != CR) && chars[i] == LF) {
      newChars[i + countLF] = CR;
      ++countLF;
    }
    newChars[i + countLF] = chars[i];
  }
  newChars.resize(chars.size() + countLF);
  return StringStorage(&newChars.front());
}

StringStorage Clipboard::removeCR(const StringStorage* str)
{
  const TCHAR* beginString = str->getString();
  const TCHAR* endString = beginString + str->getLength() + 1; // start + lenght + '\0'
  vector<TCHAR> chars(beginString, endString);
  vector<TCHAR> newChars;
  size_t countLF = 0;
  for (size_t i = 0; i < chars.size(); i++) {
    if (chars[i] != CR || i + 1 == chars.size() || chars[i + 1] != LF) {
      newChars.push_back(chars[i]);
    }
  }
  return StringStorage(&newChars.front());
}


bool Clipboard::getString(HWND hwnd, StringStorage* str)
{
  * str = _T("");

  UINT formatPriorityList[2];
  formatPriorityList[0] = CF_HDROP;
#ifdef _UNICODE
  formatPriorityList[1] = CF_UNICODETEXT;
#else
  formatPriorityList[1] = CF_TEXT;
#endif

  // (IsClipboardFormatAvailable(CF_HDROP))
  int uFormat = GetPriorityClipboardFormat(formatPriorityList, 2);
  if (uFormat == 0 || uFormat == -1) {
    _RPT(_T("Clipboard::getString GetPriorityClipboardFormat fail %u %d\n"), GetClipboardSequenceNumber(), uFormat);
    return true;
  }

  for (int i = 0; ; ) {
    if (OpenClipboard(hwnd))
      break;
  if (i > 10)
    return false;
  _RPT(_T("Clipboard::getString OpenClipboard fail %u\n"), GetClipboardSequenceNumber());
  Sleep(i * i);
  ++i;
  }

  if (uFormat == CF_HDROP) {
    HANDLE hglb = GetClipboardData(CF_HDROP);
    if (hglb != NULL) {
      const DROPFILES* lpstr = (const DROPFILES*)GlobalLock(hglb);
      if (lpstr != 0) {
        StringStorage name;

        if (lpstr->fWide) {
          for (WCHAR* p = (WCHAR*)((char*)lpstr + lpstr->pFiles); *p; ) {
            UnicodeStringStorage  path((WCHAR*)p);
            p += path.getLength() + 1;
            path.toStringStorage(&name);
            str->appendString(name.getString());
            str->appendChar('*');
          }
        }
        else {
          for (char* p = (char*)lpstr + lpstr->pFiles; *p; ) {
            AnsiStringStorage path(p);
            p += path.getLength() + 1;
            path.toStringStorage(&name);
            str->appendString(name.getString());
            str->appendChar('*');
          }
        }
        GlobalUnlock(hglb);
      }
    }
    str->appendChar('*');
    CloseClipboard();
    _RPT(_T("Clipboard::getString CF_HDROP %u \"%s\"\n"), GetClipboardSequenceNumber(), str->getString());
    return true;
  }

  str->appendChar('*');
  HANDLE hndData = GetClipboardData(uFormat);
  if (hndData) {
    TCHAR* szData = (TCHAR*)GlobalLock(hndData);
    if (szData) {
      StringStorage nativeClipboard = szData;
      GlobalUnlock(hndData);
      str->appendString(nativeClipboard.getString());
      *str = removeCR(str);
    }
  }
  CloseClipboard();
  _RPT(_T("Clipboard::getString CF_UNICODETEXT %u \"%s\"\n"), GetClipboardSequenceNumber(), str->getString()+1);
  return true;
}

static StringStorage url_encode(StringStorage *_input) {
  Utf8StringStorage utf8(_input);
  LPCSTR  input = utf8.getString();
    StringStorage temp_output_string;
    for (;  UCHAR ch = *input++; ) {
      if (isdigit(ch)) {
        temp_output_string.appendChar(ch);
      }
      else if (isalpha(ch)) {
        temp_output_string.appendChar(ch);
      }
      else if (ch == '\\')
        temp_output_string.appendChar('/');
      else if (ch == '$' || 
        ch == '-' ||
        ch == '_' ||
        ch == '.' ||
        ch == '+' ||
        ch == '!' ||
        ch == '*' ||
        ch == '\'' ||
        ch == '(' ||
        ch == ')' ||
        ch == ',') {
        temp_output_string.appendChar(ch);
      }
      else {
        TCHAR temp[4];
        _stprintf_s(temp, _T("%%%02x"), ch);
        temp_output_string.appendString(temp);
      }
    }
    return temp_output_string;
}

void Clipboard::RenderFormat(UINT format)
{
  StringStorage ServerClipboard;
  {
    std::unique_lock<std::mutex> lk(Mutex);
    ServerClipboard  = ClipText.getString();
  }

  StringStorage nativeClipboard = addCR(&ServerClipboard);

  if (nativeClipboard.isEmpty()) {
    _ASSERT(0);
    return;
  }
  if (nativeClipboard.beginsWith('*')) {
    nativeClipboard.remove(0, 1);
  }
  else if (format == CF_HDROP) {
    HGLOBAL hglb = GlobalAlloc(GMEM_MOVEABLE, sizeof(DROPFILES) + nativeClipboard.getSize());
    LPDROPFILES df = static_cast<LPDROPFILES>(GlobalLock(hglb));
    df->fWide = (sizeof(TCHAR) == sizeof(WCHAR));
    df->fNC = TRUE;
    df->pt.x = df->pt.y = 0;
    df->pFiles = sizeof(DROPFILES);

    TCHAR* pFilename = (TCHAR*)(df + 1);

    for (; ;) {
      StringStorage name;
      size_t i = nativeClipboard.findChar('*');
      if ((int)i <= 0)
        break;
      nativeClipboard.getSubstring(&name, 0, i - 1);
      nativeClipboard.remove(0, i + 1);
      {
        CopyMemory(pFilename, name.getString(), name.getSize());
        pFilename += name.getLength() + 1;
      }
    }
    *pFilename = '\0';
    GlobalUnlock(hglb);
    SetClipboardData(CF_HDROP, hglb);
    return;
  }
  else if (format == CF_UNICODETEXT || format == CF_TEXT) {
    StringStorage src;
    src.format(_T("\\\\127.0.0.1@%d\\DavWWWRoot\\"), Port);
    StringStorage dest;
    dest.format(_T("http://127.0.0.1:%d/"), Port);
    StringStorage text;

    for (; ;) {
      StringStorage name;
      size_t i = nativeClipboard.findChar('*');
      if ((int)i <= 0)
        break;
      nativeClipboard.getSubstring(&name, 0, i - 1);
      nativeClipboard.remove(0, i + 1);
      {
        text.appendString(_T("\r\n"));
        if (_tcsncmp(src.getString(), name.getString(), src.getLength()) == 0) {
          text.appendString(dest.getString());
          name.remove(0, src.getLength());
          text.appendString(url_encode(&name).getString());
        }
        else {
          text.appendString(name.getString());
        }
      }
    }
    nativeClipboard = text.getString() + 2;
    if (format == CF_UNICODETEXT) {
      int strLength = static_cast<int>(nativeClipboard.getLength()) + 1;
      int dataSize = strLength * sizeof(WCHAR);

      HGLOBAL hglb = GlobalAlloc(GMEM_MOVEABLE, dataSize);
      CopyMemory(GlobalLock(hglb), nativeClipboard.getString(), dataSize);
      GlobalUnlock(hglb);
      SetClipboardData(CF_UNICODETEXT, hglb);
    }
    else {
      AnsiStringStorage ansiClipboard(&nativeClipboard);
      int strLength = static_cast<int>(ansiClipboard.getLength()) + 1;
      int dataSize = strLength * sizeof(CHAR);

      HGLOBAL hglb = GlobalAlloc(GMEM_MOVEABLE, dataSize);
      CopyMemory(GlobalLock(hglb), ansiClipboard.getString(), dataSize);
      GlobalUnlock(hglb);
      SetClipboardData(CF_UNICODETEXT, hglb);
    }
    return;
  }
  else {
    _ASSERT(0);
    return;
  }

  if (format == CF_UNICODETEXT) {
    int strLength = static_cast<int>(nativeClipboard.getLength()) + 1;
    int dataSize = strLength * sizeof(WCHAR);

    HGLOBAL hglb = GlobalAlloc(GMEM_MOVEABLE, dataSize);
    CopyMemory(GlobalLock(hglb), nativeClipboard.getString(), dataSize);
    GlobalUnlock(hglb);
    SetClipboardData(CF_UNICODETEXT, hglb);
    return;
  }
  else if (format == CF_TEXT) {
    AnsiStringStorage ansiClipboard(&nativeClipboard);
    int strLength = static_cast<int>(ansiClipboard.getLength()) + 1;
    int dataSize = strLength * sizeof(CHAR);

    HGLOBAL hglb = GlobalAlloc(GMEM_MOVEABLE, dataSize);
    CopyMemory(GlobalLock(hglb), ansiClipboard.getString(), dataSize);
    GlobalUnlock(hglb);
    SetClipboardData(CF_UNICODETEXT, hglb);
    return;

  }
  _ASSERT(format == CF_HDROP);
}


void Clipboard::setString(HWND hwnd, const StringStorage* serverClipboard)
{
  DWORD seq = GetClipboardSequenceNumber();
  StringStorage nativeClipboard = addCR(serverClipboard);

  if (nativeClipboard.isEmpty())
    return;
  if (nativeClipboard.beginsWith('*')) {
    nativeClipboard.remove(0, 1);
  }
  else if (OpenClipboard(hwnd)) {
     EmptyClipboard();

     SetClipboardData(CF_HDROP, NULL);
     SetClipboardData(CF_UNICODETEXT, NULL);
     CloseClipboard();

    {
      std::unique_lock<std::mutex> lk(Mutex);
      ClipText = serverClipboard->getString();
    }
    _RPT(_T("Clipboard::setString CF_HDROP %u-%u \"%s\"\n"), seq, GetClipboardSequenceNumber(), serverClipboard->getString());
    return;
  }
  else
    return;

  _ASSERT(sizeof(TCHAR) == 2);
//    dataType = CF_TEXT;

  int strLength = static_cast<int>(nativeClipboard.getLength()) + 1;
  int dataSize = strLength * sizeof(TCHAR);

  if (OpenClipboard(hwnd)) {
      EmptyClipboard();
    SetClipboardData(CF_UNICODETEXT, NULL);
    CloseClipboard();
    {
      std::unique_lock<std::mutex> lk(Mutex);
      ClipText = serverClipboard->getString();
    }
    _RPT(_T("Clipboard::setString CF_UNICODETEXT %u-%u \"%s\"\n"), seq, GetClipboardSequenceNumber(), serverClipboard->getString()+1);
  }
}

StringStorage Clipboard::encodeString(const TCHAR* str, UINT* format)
{
  StringStorage text;

  if (str[0] == '*') {
    *format = 0;
    text = str + 1;
  }
  else {
    *format = (str[0] == 0) ? 0 : 1;
    text = str;
  }
  return text;
}

StringStorage Clipboard::decodeString(const TCHAR* str, UINT format, const TCHAR* host)
{
  StringStorage text;
  //StringStorage input(str);
 
  if (format == 1) {
    Str<const TCHAR> slice(str);
    for (; ;) {
        Str<const TCHAR> i(slice.begin(), slice.find('*'));
        slice = Str<const TCHAR>(i.end()+1, slice.end());

        if (i.empty())
            break;
        StringStorage temp;
        Str<const TCHAR> j;
      
        if (isalpha(i[0]) && i[1] == ':' && i[2] == '\\') {
            TCHAR drive = i[0];
            i.m_begin += 3;
            if (Port)
                temp.format(_T("\\\\127.0.0.1@%d\\DavWWWRoot\\%s#%c\\%.*s*"), Port, host, drive, i.len(), i.begin());
            else
                temp.format(_T("\\\\%s\\%c\\%.*s*"), host, tolower(drive), i.len(), i.begin());
        }
        else if (Port && i[0] == '\\' && i[1] == '\\' && !(j = Str<const TCHAR>( std::find(&i[2], i.end(), '\\'), i.end() )).empty()) {
            if (_tcsncmp(&j[1], _T("DavWWWRoot\\"), 11) == 0 &&
                _tcsncmp(std::find(&j[12], j.end(), '\\') - 10, _T("DavWWWRoot"), 10) == 0) {
            }
            else {
                i = Str<const TCHAR>(&i[2], j.begin());
                ++j.m_begin;
                temp.format(_T("\\\\127.0.0.1@%d\\DavWWWRoot\\%s#%.*s#%.*s*"), Port, host, i.len(), i.begin(), j.len(),  j.begin());
            }
        }
        else 
            temp.format(_T("%.*s*"), i.len(), i.begin()); 

        text.appendString(temp.getString());
    }
    text.appendChar('*');
  }
  else if (str[0]) {
    text.appendChar('*');
    text.appendString(str);
  }

  return removeCR(&text);
}

void Clipboard::setPort(int port)
{
  Port = port;
}