// Copyright (C) 2009,2010,2011,2012 GlavSoft LLC.
// All rights reserved.
//
//-------------------------------------------------------------------------
// This file is part of the TightVNC software.  Please visit our Web site:
//
//                       http://www.tightvnc.com/
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//-------------------------------------------------------------------------
//

#include "WebDavOperation.h"
#include "RfbInputGate.h"
#include "RfbOutputGate.h"
#include "thread/AutoLock.h"
#include <mutex>

static   LONG Seq;
void _SendWebDav(LPCTSTR host, WebDavOperation* webdav);
uintptr_t SendWebDav(LPCTSTR host, WebDavOperation* webdav);

#define BUFSIZE 64 * 1024
#define MIN_BUFSIZE 1024
#if BUFSIZE < MIN_BUFSIZE
#error BUFSIZE is too small
#endif


WebDavOperation::WebDavOperation(int code, LPVOID param, INT32 result)
  : m_code(code),
  m_param(param),
  m_result(result)
{
	m_seq = InterlockedIncrement((volatile LONG*)&Seq);
    m_event = CreateEvent(NULL, FALSE, FALSE, NULL);
    m_error = ERROR_ACCESS_DENIED;
}

WebDavOperation::~WebDavOperation()
{
  CloseHandle(m_event);
}

void WebDavOperation::OnDisconnected()
{
  //m_cv.notify_all();
  SetEvent(m_event);
}

void WebDavOperation::Decompose(StringStorage* src, StringStorage* dest)
{
    Str<const TCHAR> slice(src->getString());
    Str<const TCHAR> prefix(slice.begin(), slice.find(_T("\\/")));
    Str<const TCHAR> name(prefix.begin(), prefix.find('#'));
    if (name.end() == prefix.end())
        dest->format(_T("%.*s:\\%s"), prefix.len(), prefix.begin(), (prefix.end() == slice.end()) ? _T("") : prefix.end() + 1);
    else
        dest->format(_T("\\\\%.*s\\%s"), name.len(), name.begin(), name.end()+1);
}

static BOOL SetPrivilege(
  HANDLE hToken,          // access token handle
  LPCTSTR lpszPrivilege,  // name of privilege to enable/disable
  BOOL bEnablePrivilege   // to enable or disable privilege
)
{

  TOKEN_PRIVILEGES tp;
  LUID luid;

  if (!LookupPrivilegeValue(
    NULL,            // lookup privilege on local system
    lpszPrivilege,   // privilege to lookup 
    &luid))        // receives LUID of privilege
  {
    printf("LookupPrivilegeValue error: %u\n", GetLastError());
    return FALSE;
  }

  tp.PrivilegeCount = 1;
  tp.Privileges[0].Luid = luid;
  if (bEnablePrivilege)
    tp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
  else
    tp.Privileges[0].Attributes = 0;

  // Enable the privilege or disable all privileges.

  if (!AdjustTokenPrivileges(
    hToken,
    FALSE,
    &tp,
    sizeof(TOKEN_PRIVILEGES),
    (PTOKEN_PRIVILEGES)NULL,
    (PDWORD)NULL))
  {
    printf("AdjustTokenPrivileges error: %u\n", GetLastError());
    return FALSE;
  }

  if (GetLastError() == ERROR_NOT_ALL_ASSIGNED)

  {
    printf("The token does not have the specified privilege. \n");
    return FALSE;
  }

  return TRUE;
}

BOOL WebDavOperation::EnablePrivilege()
{

  HANDLE hToken;

  if (OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken)) {
    SetPrivilege(hToken, SE_BACKUP_NAME, TRUE);
    SetPrivilege(hToken, SE_RESTORE_NAME, TRUE);
    CloseHandle(hToken);
    return TRUE;
  }
  return FALSE;
}

  DWORD WebDav::_CloseHandle(INT32 seq)
  {
    std::unique_lock<std::mutex> lk(HandleMutex);
    for (auto it = HandleTable.begin(); it != HandleTable.end(); ++it) {
      auto p = *it;
      if (p.seq == seq) {
        HandleTable.erase(it);
        if (!CloseHandle(p.handle)) 
          return GetLastError();
        else
          return 0;
      }
    }
    return ERROR_INVALID_HANDLE;
  }

  DWORD WebDav::_FindClose(INT32 seq)
  {
   // _RPT(L"FindClose %d\n", seq);
    std::unique_lock<std::mutex> lk(HandleMutex);
    for (auto it = FindHandleTable.begin(); it != FindHandleTable.end(); ++it) {
      auto p = *it;
      if (p.seq == seq) {
        FindHandleTable.erase(it);
        if (!FindClose(p.handle))
          return GetLastError();
        else
          return 0;
      }
    }
    return ERROR_INVALID_HANDLE;
  }


HANDLE WebDav::GetHandle(INT32 seq)
{
  std::unique_lock<std::mutex> lk(HandleMutex);
  for (auto p : HandleTable) {
    if (p.seq == seq)
      return p.handle;
  }
  return INVALID_HANDLE_VALUE;
}

HANDLE WebDav::GetFindHandle(INT32 seq)
{
  std::unique_lock<std::mutex> lk(HandleMutex);
  for (auto p : FindHandleTable) {
    if (p.seq == seq)
      return p.handle;
  }
  return INVALID_HANDLE_VALUE;
}

void WebDav::AddHandleHost(INT32 seq, LPCTSTR host) 
{
    std::unique_lock<std::mutex> lk(HandleMapMutex);
    HandleMap.push_back(HandleHost{ seq, host });
}


StringStorage WebDav::RemoveHandleHost(INT32 seq)
{
    StringStorage host;
    std::unique_lock<std::mutex> lk(HandleMapMutex);
    for (auto it = HandleMap.begin(); it != HandleMap.end(); ++it) {
        if (it->seq == seq) {
            host = it->host;
            HandleMap.erase(it);
            return host;
        }
    }
    return host;
}

StringStorage  WebDav::GetHandleHost(INT32 seq)
{
    std::unique_lock<std::mutex> lk(HandleMapMutex);
    for (auto it = HandleMap.begin(); it != HandleMap.end(); ++it) {
        if (it->seq == seq) {
            return it->host;
        }
    }
    return _T("");
}


LPCTSTR WebDavOperation::GetHost(LPCTSTR lpFileName, StringStorage* server)
{
  StringStorage name(lpFileName + 2);
  size_t i = name.findChar('#');
  name.getSubstring(server, 0, i - 1);
  return lpFileName + i + 3;
}

std::mutex WebDav::HandleMapMutex;
std::vector<WebDav::HandleHost> WebDav::HandleMap;

WebDav::~WebDav()
{
  OnDisconnect();

  std::unique_lock<std::mutex> lk(HandleMutex);
  for (auto h : HandleTable) {
    CloseHandle(h.handle);
  }
  HandleTable.clear();
  for (auto h : FindHandleTable) {
    //_RPT(L"CloseHandle %d %d\n", h.seq, h.handle);
    FindClose(h.handle);
  }
  FindHandleTable.clear();
}

void WebDav::OnDisconnect()
{
  std::unique_lock<std::mutex> lk(webdavMutex);
  for (auto p : webdavTable) {
    p->OnDisconnected();
  }
  webdavTable.clear();

}

void WebDav::OnFail(DataInputStream* input)
{
  StringStorage host;
  input->readUTF8(&host);

  {
      std::unique_lock<std::mutex> lk(webdavMutex);
      for (auto it = webdavTable.begin(); it != webdavTable.end(); ) {

          auto p = *it;
          if (_tcscmp(p->m_host, host.getString()) == 0) {
              it = webdavTable.erase(it);
              SetEvent(p->m_event);
          }
          else
              ++it;
      }
  }
  {
      std::unique_lock<std::mutex> lk(HandleMapMutex);
      for (auto it = HandleMap.begin(); it != HandleMap.end(); ) {
          if (_tcscmp(it->host.getString(), host.getString()) == 0) {
              it = HandleMap.erase(it);
          }
          else
              ++it;
      }
  }

}


void WebDav::OnRequest(UINT32 repCode, DataInputStream* input, RfbOutputGate* output)
{
  INT32 seq = input->readInt32();
  UINT8 code = input->readUInt8();
  INT32 result;
  UINT32 error;

  switch (code) {
  case GetFileAttributesEx_:
  {
      StringStorage fileName;
      StringStorage name;
      input->readUTF8(&fileName);
      DWORD dwFlagsAndAttributes = input->readUInt32();
      WebDavOperation::Decompose(&fileName, &name);
      WIN32_FILE_ATTRIBUTE_DATA fileStat;
      ZeroMemory(&fileStat, sizeof(fileStat));
      result = GetFileAttributesEx(name.getString(), GetFileExInfoStandard, &fileStat);
      StringStorage basename;
      if (result && (fileStat.dwFileAttributes & FILE_ATTRIBUTE_REPARSE_POINT) == 0) {
          error = 0;
      }
      else if ((error = GetLastError()) == ERROR_SHARING_VIOLATION) {
          WIN32_FIND_DATAW  findFileData;

          HANDLE h = FindFirstFile(name.getString(), &findFileData);
          if (h != INVALID_HANDLE_VALUE) {
              result = 1;
              error = 0;
              if ((findFileData.dwFileAttributes & FILE_ATTRIBUTE_REPARSE_POINT) != 0
                  && findFileData.dwReserved0 != IO_REPARSE_TAG_SYMLINK
                  && findFileData.dwReserved0 != IO_REPARSE_TAG_MOUNT_POINT) {
                      findFileData.dwFileAttributes &= ~FILE_ATTRIBUTE_REPARSE_POINT;
              }
              fileStat.dwFileAttributes = findFileData.dwFileAttributes;
              fileStat.ftCreationTime = findFileData.ftCreationTime;
              fileStat.ftLastAccessTime = findFileData.ftLastAccessTime;
              fileStat.ftLastWriteTime = findFileData.ftLastWriteTime;
              fileStat.nFileSizeHigh = findFileData.nFileSizeHigh;
              fileStat.nFileSizeLow = findFileData.nFileSizeLow;
              FindClose(h);
          }
          else {
              error = GetLastError();
          }
      }
      else {
          BY_HANDLE_FILE_INFORMATION  fileInformation;
          FILE_ATTRIBUTE_TAG_INFO ti;
          ti.ReparseTag = 0;

          HANDLE h = CreateFileW(name.getString(), 0, 0, NULL, OPEN_EXISTING, dwFlagsAndAttributes, 0);
          error = GetLastError();
          if (h != INVALID_HANDLE_VALUE) {
              result = GetFileInformationByHandle(h, &fileInformation);
              error = GetLastError();
              if (result) {
                  result = GetFileInformationByHandleEx(h, FileAttributeTagInfo, &ti, sizeof(ti));
                  error = GetLastError();
                  if (!result && error == ERROR_INVALID_PARAMETER) {
                      ti.ReparseTag = 0;
                      result = 1;
                      error = 0;
                  }
                  if (result) {
                      error = 0;
                      if ((fileInformation.dwFileAttributes & FILE_ATTRIBUTE_REPARSE_POINT) != 0
                          && ti.ReparseTag != IO_REPARSE_TAG_SYMLINK
                          && ti.ReparseTag != IO_REPARSE_TAG_MOUNT_POINT) {
                              fileInformation.dwFileAttributes &= ~FILE_ATTRIBUTE_REPARSE_POINT;
                      }
                      fileStat.dwFileAttributes = fileInformation.dwFileAttributes;
                      fileStat.ftCreationTime = fileInformation.ftCreationTime;
                      fileStat.ftLastAccessTime = fileInformation.ftLastAccessTime;
                      fileStat.ftLastWriteTime = fileInformation.ftLastWriteTime;
                      fileStat.nFileSizeHigh = fileInformation.nFileSizeHigh;
                      fileStat.nFileSizeLow = fileInformation.nFileSizeLow;
                  }
              }
              CloseHandle(h);
          }
          else
              result = 0;
      }

      AutoLock al(output);
      output->writeUInt8(repCode); // type
      output->writeInt32(seq);
      output->writeInt32(result);
      output->writeUInt32(error);
      output->writeFully(&fileStat, sizeof(fileStat));
      output->flush();
  }
  break;
  case FindFirstFile_:
  {
      StringStorage fileName;
      StringStorage name;
      WIN32_FIND_DATAW  findFileData;

      input->readUTF8(&fileName);
      WebDavOperation::Decompose(&fileName, &name);

      StringStorage mask;
      LPCTSTR path = name.getString();
      {
          // strip trailing '\\'
          Str<const TCHAR> slice(path);
          slice.m_end = slice.rfind_not(_T("\\/"));
          mask.format(_T("%.*s\\*"), slice.len(), slice.begin());
      }
      HANDLE h = FindFirstFile(mask.getString(), &findFileData);
      if (h == INVALID_HANDLE_VALUE) {
          error = GetLastError();
          WIN32_FILE_ATTRIBUTE_DATA fa;
          if (error != ERROR_FILE_NOT_FOUND) {
          }
          else if (!GetFileAttributesEx(name.getString(), GetFileExInfoStandard, &fa)) {
              error = GetLastError();
          }
          else if ((fa.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) == 0) {
          }
          else {
              error = 0; // empty directory
          }
      }
      else {
          error = 0;
      }
      while (h != INVALID_HANDLE_VALUE) {
        if (_tcscmp(findFileData.cFileName, _T(".")) != 0 && _tcscmp(findFileData.cFileName, _T("..")) != 0)
            break;
        if (!FindNextFileW(h, &findFileData)) {
            FindClose(h);
            h = INVALID_HANDLE_VALUE;
        }
      }

      if (h != INVALID_HANDLE_VALUE) {
          result = seq;
          //_RPT(L"FindFirstFileW %d %d\n", seq, h);
          std::unique_lock<std::mutex> lk(HandleMutex);
          FindHandleTable.push_back(_HANDLE{ seq, h });
      }
      else
          result = (INT32)INVALID_HANDLE_VALUE;

      AutoLock al(output);
      output->writeUInt8(repCode); // type
      output->writeInt32(seq);
      output->writeInt32(result);
      output->writeUInt32(error);
      output->writeFully(&findFileData, sizeof(findFileData)); // pad
      output->flush();
  }
  break;
  case FindClose_:
  {
      INT32 hFile = input->readInt32();
      error = WebDav::_FindClose(hFile);
      result = (error == 0);

      AutoLock al(output);
      output->writeUInt8(repCode); // type
      output->writeInt32(seq);
      output->writeInt32(result);
      output->writeUInt32(error);
      output->flush();
  }
  break;
  case FindNextFile_:
  {
      WIN32_FIND_DATAW  findFileData;

      INT32 hFile = input->readInt32();
      HANDLE handle = GetFindHandle(hFile);

      for ( ; ; ) {
          result = FindNextFileW(handle, &findFileData);
          if (!result) {
              error = GetLastError();
              _FindClose(hFile);
              break;
          }
          if (_tcscmp(findFileData.cFileName, _T(".")) != 0 && _tcscmp(findFileData.cFileName, _T("..")) != 0) {
              error = 0;
              break;
          }
      }

      AutoLock al(output);
      output->writeUInt8(repCode); // type
      output->writeInt32(seq);
      output->writeInt32(result);
      output->writeUInt32(error);
      output->writeFully(&findFileData, sizeof(findFileData)); // pad
      output->flush();
  }
  break;
  case CreateFile_:
  {
      StringStorage fileName;
      StringStorage name;
      input->readUTF8(&fileName);
      WebDavOperation::Decompose(&fileName, &name);

      DWORD dwDesiredAccess = input->readUInt32();
      DWORD dwShareMode = input->readUInt32();
      DWORD dwCreationDisposition = input->readUInt32();
      DWORD dwFlagsAndAttributes = input->readUInt32();

      error = 0;
      HANDLE h;
      if (dwFlagsAndAttributes & FILE_ATTRIBUTE_READONLY) {
          if (dwCreationDisposition == CREATE_ALWAYS) {
              h = CreateFile(name.getString(), dwDesiredAccess, dwShareMode, NULL, TRUNCATE_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
              if (h == INVALID_HANDLE_VALUE) {
                  error = GetLastError();
                  switch (error) {
                  case ERROR_FILE_NOT_FOUND:
                  case ERROR_BAD_NETPATH:
                  case ERROR_PATH_NOT_FOUND:
                      error = 0;
                      break;
                  }
              }
          }
      }
      if (!error) {
          h = CreateFileW(name.getString(), dwDesiredAccess, dwShareMode, NULL, dwCreationDisposition, dwFlagsAndAttributes, 0);
          if (h == INVALID_HANDLE_VALUE) {
              error = GetLastError();
          }
      }
      if (h != INVALID_HANDLE_VALUE) {
          std::unique_lock<std::mutex> lk(HandleMutex);
          HandleTable.push_back(_HANDLE{ seq, h });
          result = seq;
      }
      else
          result = (INT32)INVALID_HANDLE_VALUE;

      AutoLock al(output);
      output->writeUInt8(repCode); // type
      output->writeInt32(seq);
      output->writeInt32(result);
      output->writeUInt32(error);
      output->flush();
  }
  break;
  case CloseHandle_:
  {
      INT32 hFile = input->readInt32();
      error = WebDav::_CloseHandle(hFile);
      result = (error == 0);

      AutoLock al(output);
      output->writeUInt8(repCode); // type
      output->writeInt32(seq);
      output->writeInt32(result);
      output->writeUInt32(error);
      output->flush();
  }
  break;
  case GetFileInformationByHandle_:
  {
        WIN32_FILE_ATTRIBUTE_DATA fd;
        BY_HANDLE_FILE_INFORMATION  fileInformation;
        FILE_ATTRIBUTE_TAG_INFO ti;
        ti.ReparseTag = 0;

        INT32 hFile = input->readInt32();
        HANDLE handle = GetHandle(hFile);
        result = GetFileInformationByHandle(handle, &fileInformation);
        error = GetLastError();
        if (result) {
            result = GetFileInformationByHandleEx(handle, FileAttributeTagInfo, &ti, sizeof(ti));
            error = GetLastError();
            if (!result && error == ERROR_INVALID_PARAMETER) {
                ti.ReparseTag = 0;
                result = 1;
                error = 0;
            }
        }
        fd.dwFileAttributes = fileInformation.dwFileAttributes;
        fd.ftCreationTime = fileInformation.ftCreationTime;
        fd.ftLastAccessTime = fileInformation.ftLastAccessTime;
        fd.ftLastWriteTime = fileInformation.ftLastWriteTime;
        fd.nFileSizeHigh = fileInformation.nFileSizeHigh;
        fd.nFileSizeLow = fileInformation.nFileSizeLow;
        if ((fd.dwFileAttributes & FILE_ATTRIBUTE_REPARSE_POINT) != 0
            && ti.ReparseTag != IO_REPARSE_TAG_SYMLINK
            && ti.ReparseTag != IO_REPARSE_TAG_MOUNT_POINT) {
                fd.dwFileAttributes &= ~FILE_ATTRIBUTE_REPARSE_POINT;
        }
      AutoLock al(output);
      output->writeUInt8(repCode); // type
      output->writeInt32(seq);
      output->writeInt32(result);
      output->writeUInt32(error);
      output->writeFully(&fd, sizeof(fd));
      output->flush();
  }
  break;
  case ReadFile_:
  {
      INT32 hFile = input->readInt32();
      HANDLE handle = GetHandle(hFile);
      DWORD nNumberOfBytesToRead = input->readUInt32();

      if (nNumberOfBytesToRead > BUFSIZE)
          nNumberOfBytesToRead = BUFSIZE;
      void* buffer = _malloca(nNumberOfBytesToRead);
      DWORD numberOfBytesRead = 0;
      result = ReadFile(handle, buffer, nNumberOfBytesToRead, &numberOfBytesRead, NULL);
      error = GetLastError();

      AutoLock al(output);
      output->writeUInt8(repCode); // type
      output->writeInt32(seq);
      output->writeInt32(result);
      output->writeUInt32(error);
      output->writeUInt32(numberOfBytesRead);
      output->writeFully(buffer, numberOfBytesRead);
      output->flush();
      _freea(buffer);
  }
  break;
  case  SetFilePointerEx_:
  {
      INT32 hFile = input->readInt32();
      HANDLE handle = GetHandle(hFile);
      LARGE_INTEGER  liDistanceToMove;
      liDistanceToMove.QuadPart = input->readInt64();
      LARGE_INTEGER lpNewFilePointer;
      DWORD          dwMoveMethod = input->readUInt32();

      result = SetFilePointerEx(handle, liDistanceToMove, &lpNewFilePointer, dwMoveMethod);
      error = GetLastError();

      AutoLock al(output);
      output->writeUInt8(repCode); // type
      output->writeInt32(seq);
      output->writeInt32(result);
      output->writeUInt32(error);
      output->writeInt64(lpNewFilePointer.QuadPart);
      output->flush();
  }
  break;
  }

}
void WebDav::SendRequest(UINT reqCode, WebDavOperation* webdav, RfbOutputGate* output)
{
  {
  std::unique_lock<std::mutex> lk(webdavMutex);
  webdavTable.push_back(webdav);
  }

  //m_fileTransferListeners->addListener(p);
  auto m_webdav = webdav;

  AutoLock al(output);
  output->writeUInt8(reqCode);
  output->writeUInt32(m_webdav->m_seq);
  output->writeUInt8(m_webdav->m_code);
  switch (m_webdav->m_code) {
  case GetFileAttributesEx_:
      output->writeUTF8(((GetFileAttributesEx_Param*)m_webdav->m_param)->name);
      output->writeUInt32(((GetFileAttributesEx_Param*)m_webdav->m_param)->dwFlagsAndAttributes);
      break;
  case FindFirstFile_:
      output->writeUTF8(((FindFirstFile_Param*)m_webdav->m_param)->lpFileName);
      break;
  case FindClose_:
      output->writeInt32(((FindClose_Param*)m_webdav->m_param)->hFile);
      break;
  case FindNextFile_:
      output->writeInt32(((FindNextFile_Param*)m_webdav->m_param)->hFindFile);
      break;
  case CreateFile_:
      output->writeUTF8(((CreateFile_Param*)m_webdav->m_param)->lpFileName);
      output->writeUInt32(((CreateFile_Param*)m_webdav->m_param)->dwDesiredAccess);
      output->writeUInt32(((CreateFile_Param*)m_webdav->m_param)->dwShareMode);
      output->writeUInt32(((CreateFile_Param*)m_webdav->m_param)->dwCreationDisposition);
      output->writeUInt32(((CreateFile_Param*)m_webdav->m_param)->dwFlagsAndAttributes);
      break;
  case CloseHandle_:
      output->writeInt32(((CloseHandle_Param*)m_webdav->m_param)->hFile);
      break;
  case GetFileInformationByHandle_:
      output->writeInt32(((GetFileInformationByHandle_Param*)m_webdav->m_param)->hFile);
      break;
  case ReadFile_:
      output->writeInt32(((ReadFile_Param*)m_webdav->m_param)->hFile);
      output->writeUInt32(((ReadFile_Param*)m_webdav->m_param)->nNumberOfBytesToRead -
          *((ReadFile_Param*)m_webdav->m_param)->lpNumberOfBytesRead);
      break;
  case SetFilePointerEx_:
      output->writeInt32(((SetFilePointerEx_Param*)m_webdav->m_param)->hFile);
      output->writeInt64(((SetFilePointerEx_Param*)m_webdav->m_param)->liDistanceToMove);
      output->writeUInt32(((SetFilePointerEx_Param*)m_webdav->m_param)->dwMoveMethod);
      break;
  }
  output->flush();
}

void WebDav::OnReply(DataInputStream* input)
{
  LONG seq = input->readInt32();
  webdavMutex.lock();
  for (auto it = webdavTable.begin(); it != webdavTable.end(); ++it) {

    auto p = *it;
    if (p->m_seq == seq) {
      webdavTable.erase(it);
      webdavMutex.unlock();
      p->m_result = input->readInt32();
      p->m_error = input->readUInt32();
      switch (p->m_code) {
      case GetFileAttributesEx_:
      {
          input->readFully(((GetFileAttributesEx_Param*)p->m_param)->lpFileInformation, sizeof(WIN32_FILE_ATTRIBUTE_DATA));
      }
      break;
      case FindClose_:
          break;
      case FindFirstFile_:
      {
          input->readFully(((FindFirstFile_Param*)p->m_param)->lpFindFileData, sizeof(WIN32_FIND_DATAW));
      }
      break;
      case FindNextFile_:
      {
          input->readFully(((FindNextFile_Param*)p->m_param)->lpFindFileData, sizeof(WIN32_FIND_DATAW));
      }
      break;
      case CreateFile_:
          break;
      case CloseHandle_:
          break;
      case GetFileInformationByHandle_:
      {
          input->readFully(((GetFileInformationByHandle_Param*)p->m_param)->lpFileInformation, sizeof(WIN32_FILE_ATTRIBUTE_DATA));
      }
      break;
      case ReadFile_:
      {
          DWORD  numberOfBytesRead = input->readUInt32();
          input->readFully(
              (char*)((ReadFile_Param*)p->m_param)->lpBuffer +
              *((ReadFile_Param*)p->m_param)->lpNumberOfBytesRead,
              numberOfBytesRead);

          *((ReadFile_Param*)p->m_param)->lpNumberOfBytesRead += numberOfBytesRead;

          if (p->m_result && MIN_BUFSIZE <= numberOfBytesRead &&
              ((ReadFile_Param*)p->m_param)->nNumberOfBytesToRead >
              *((ReadFile_Param*)p->m_param)->lpNumberOfBytesRead)
              _SendWebDav(p->m_host, p);
          else {
              SetEvent(p->m_event);
          }
          return;
      }
      break;
      case SetFilePointerEx_:
      {
          ((SetFilePointerEx_Param*)p->m_param)->lpNewFilePointer->QuadPart = input->readInt64();
      }
      break;


      }
      SetEvent(p->m_event);
      return;
    }
  }
  webdavMutex.unlock();

}

extern "C" __declspec(dllexport) BOOL  WINAPI  GetFileAttributesEx_(LPCWSTR name, WIN32_FILE_ATTRIBUTE_DATA *lpFileInformation, DWORD dwFlagsAndAttributes)
{
    StringStorage server;
    name = WebDavOperation::GetHost(name, &server);

    WebDav::GetFileAttributesEx_Param param = { name, lpFileInformation, dwFlagsAndAttributes};
    WebDavOperation webdav(WebDav::GetFileAttributesEx_, &param, FALSE);

    SendWebDav(server.getString(), &webdav);
    _RPT(L"GetFileAttributesEx_(%s)->%d\n", name, webdav.m_result);
    return webdav.m_result;
}

extern "C" __declspec(dllexport) HANDLE  WINAPI  FindFirstFile_(LPCWSTR lpFileName, LPWIN32_FIND_DATAW  lpFindFileData)
{
    StringStorage server;
    lpFileName = WebDavOperation::GetHost(lpFileName, &server);

    WebDav::FindFirstFile_Param param = { lpFileName, lpFindFileData };
    WebDavOperation webdav(WebDav::FindFirstFile_, &param, (INT32)INVALID_HANDLE_VALUE);

    SendWebDav(server.getString(), &webdav);
    if ((HANDLE)webdav.m_result != INVALID_HANDLE_VALUE) {
        WebDav::AddHandleHost(webdav.m_result, webdav.m_host);
    }
    _RPT(L"FindFirstFile_(%s)->%x\n", lpFileName, webdav.m_result);
    return (HANDLE) webdav.m_result;
}

extern "C" __declspec(dllexport) BOOL  WINAPI  FindClose_(
    HANDLE hFile
)
{
    WebDav::CloseHandle_Param param = { (INT32)hFile };
    WebDavOperation webdav(WebDav::FindClose_, &param, FALSE);

    StringStorage host = WebDav::RemoveHandleHost((INT32)hFile);
    SendWebDav(host.getString(), &webdav);
    _RPT(L"FindClose_(%s, %x)->%d\n", webdav.m_host, hFile, webdav.m_result);
    return webdav.m_result;
}

extern "C" __declspec(dllexport) BOOL  WINAPI  FindNextFile_(
    HANDLE             hFindFile,
    LPWIN32_FIND_DATAW lpFindFileData)
{

    WebDav::FindNextFile_Param param = { (UINT32)hFindFile, lpFindFileData };
    WebDavOperation webdav(WebDav::FindNextFile_, &param, FALSE);

    StringStorage host = WebDav::GetHandleHost((INT32)hFindFile);
    SendWebDav(host.getString(), &webdav);
    if (!webdav.m_result) {
        _RPT(L"FindNextFile_(%s, %x)->%d\n", webdav.m_host, hFindFile, webdav.m_result);
    }
    return webdav.m_result;
}

extern "C" __declspec(dllexport) HANDLE  WINAPI  CreateFile_(
    LPCWSTR               lpFileName,
    DWORD                 dwDesiredAccess,
    DWORD                 dwShareMode,
    //LPSECURITY_ATTRIBUTES lpSecurityAttributes,
    DWORD                 dwCreationDisposition,
    DWORD                 dwFlagsAndAttributes
    //HANDLE                hTemplateFile
    )
{

    StringStorage server;
    lpFileName = WebDavOperation::GetHost(lpFileName, &server);

    WebDav::CreateFile_Param param = { lpFileName, dwDesiredAccess, dwShareMode, dwCreationDisposition,  dwFlagsAndAttributes };
    WebDavOperation webdav(WebDav::CreateFile_, &param, (INT32)INVALID_HANDLE_VALUE);

    SendWebDav(server.getString(), &webdav);
    if ((HANDLE)webdav.m_result != INVALID_HANDLE_VALUE) {
        WebDav::AddHandleHost(webdav.m_result, webdav.m_host);
    }
    _RPT(L"CreateFile_(%s)->%x\n", lpFileName, webdav.m_result);
    return (HANDLE) webdav.m_result;
}

extern "C" __declspec(dllexport) BOOL  WINAPI  CloseHandle_(
    HANDLE hFile
)
{
    WebDav::CloseHandle_Param param = { (INT32)hFile };
    WebDavOperation webdav(WebDav::CloseHandle_, &param, FALSE);

    StringStorage host = WebDav::RemoveHandleHost((INT32)hFile);
    SendWebDav(host.getString(), &webdav);
    _RPT(L"CloseHandle_(%s, %x)->%d\n", webdav.m_host, hFile, webdav.m_result);
    return webdav.m_result;
}

extern "C" __declspec(dllexport) BOOL  WINAPI  GetFileInformationByHandle_(HANDLE hFile,   LPWIN32_FILE_ATTRIBUTE_DATA lpFileInformation)
{
    WebDav::GetFileInformationByHandle_Param param = { (INT32)hFile,  lpFileInformation};
    WebDavOperation webdav(WebDav::GetFileInformationByHandle_, &param, FALSE);

//    _RPT(L"_GetFileInformationByHandle begin (%d) %d %d\n", webdav.m_seq, GetCurrentThreadId(), hFile);
    StringStorage host = WebDav::GetHandleHost((INT32)hFile);
    SendWebDav(host.getString(), &webdav);
    _RPT(L"GetFileInformationByHandle_(%s, %x)->%d\n", webdav.m_host, hFile, webdav.m_result);
    return webdav.m_result;
}

extern "C" __declspec(dllexport) BOOL  WINAPI  ReadFile_(
    HANDLE         hFile,
    LPVOID       lpBuffer,
    DWORD        nNumberOfBytesToRead,
    LPDWORD      lpNumberOfBytesRead,
    LPOVERLAPPED lpOverlapped
)
{
    WebDav::ReadFile_Param param = { (INT32)hFile, lpBuffer, nNumberOfBytesToRead, lpNumberOfBytesRead, lpOverlapped };
    WebDavOperation webdav(WebDav::ReadFile_, &param, FALSE);

    _ASSERT(lpOverlapped == NULL);
    *lpNumberOfBytesRead = 0;
    StringStorage host = WebDav::GetHandleHost((INT32)hFile);
    SendWebDav(host.getString(), &webdav);
    _RPT(L"ReadFile_(%s, %x, %d, %d)->%d\n", webdav.m_host, hFile, nNumberOfBytesToRead, *lpNumberOfBytesRead, webdav.m_result);
    return webdav.m_result;
}

extern "C" __declspec(dllexport) BOOL  WINAPI  SetFilePointerEx_(
    HANDLE         hFile,
    LARGE_INTEGER  liDistanceToMove,
    PLARGE_INTEGER lpNewFilePointer,
    DWORD          dwMoveMethod
)
{
    WebDav::SetFilePointerEx_Param param = { (INT32)hFile, liDistanceToMove.QuadPart, lpNewFilePointer, dwMoveMethod };
    WebDavOperation webdav(WebDav::SetFilePointerEx_, &param, FALSE);

    StringStorage host = WebDav::GetHandleHost((INT32)hFile);
    SendWebDav(host.getString(), &webdav);
    _RPT(L"SetFilePointerEx_(%s, %x, %lld, %lld, %d)->%d\n", webdav.m_host, hFile, liDistanceToMove, *lpNewFilePointer, dwMoveMethod, webdav.m_result);
    return webdav.m_result;
}


