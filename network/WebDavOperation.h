// Copyright (C) 2009,2010,2011,2012 GlavSoft LLC.
// All rights reserved.
//
//-------------------------------------------------------------------------
// This file is part of the TightVNC software.  Please visit our Web site:
//
//                       http://www.tightvnc.com/
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//-------------------------------------------------------------------------
//

#ifndef _WEB_DAV_OPERATION_H_
#define _WEB_DAV_OPERATION_H_

//#include "windows.h"
//#include <condition_variable>
#include "util/StringStorage.h"
#include <MSWSock.h>
#include <mutex>


class DataInputStream;
class RfbOutputGate;
//
// File operation that used for receiving file list
// from remote file system.
//

class WebDavOperation 
{
public:



  WebDavOperation(int code, LPVOID param, INT32 result);
  ~WebDavOperation();
  void OnDisconnected();

  LONG m_seq;
  int m_code;
  LPVOID m_param;
  INT32 m_result;
  DWORD m_error;
  LPCTSTR	 m_host;


  HANDLE m_event;



  static void Decompose(StringStorage* src, StringStorage* dest);
  static BOOL WebDavOperation::EnablePrivilege();
  static LPCTSTR GetHost(LPCTSTR lpFileName, StringStorage* server);
};

class WebDav
{
public:

  // Invented values to support what package os expects.
  enum {
	O_RDONLY = 0x00000,
	O_WRONLY = 0x00001,
	O_RDWR = 0x00002,
	O_CREAT = 0x00040,
	O_EXCL = 0x00080,
	O_NOCTTY = 0x00100,
	O_TRUNC = 0x00200,
	O_NONBLOCK = 0x00800,
	O_APPEND = 0x00400,
	O_SYNC = 0x01000,
	O_ASYNC = 0x02000,
	O_CLOEXEC = 0x80000,
  };

  enum {
	  GetFileAttributesEx_,
	  FindFirstFile_,
	  FindNextFile_,
	  FindClose_,
	  CreateFile_,
	  CloseHandle_,
	  GetFileInformationByHandle_,
	  ReadFile_,
	  SetFilePointerEx_,
  };

  using   GetFileAttributesEx_Param = struct {
	  LPCWSTR             name;
	  WIN32_FILE_ATTRIBUTE_DATA *lpFileInformation;
	  DWORD dwFlagsAndAttributes;
  };
  using FindFirstFile_Param = struct {
	  LPCWSTR             lpFileName;
	  LPWIN32_FIND_DATAW  lpFindFileData;
  };
  using   FindClose_Param = struct {
	  INT32 hFile;
  };
  using FindNextFile_Param = struct {
	  UINT32             hFindFile;
	  LPWIN32_FIND_DATAW lpFindFileData;
  };
  using   CreateFile_Param = struct {
	  LPCWSTR               lpFileName;
	  DWORD                 dwDesiredAccess;
	  DWORD                 dwShareMode;
	  //LPSECURITY_ATTRIBUTES lpSecurityAttributes;
	  DWORD                 dwCreationDisposition;
	  DWORD                 dwFlagsAndAttributes;
	  //INT64                hTemplateFile;
  };
  using  CloseHandle_Param = struct {
	  INT32 hFile;
  };
  using GetFileInformationByHandle_Param = struct {
	  INT32 hFile;
	  WIN32_FILE_ATTRIBUTE_DATA* lpFileInformation;
  };
  using   ReadFile_Param = struct {
	  INT32         hFile;
	  LPVOID       lpBuffer;
	  DWORD        nNumberOfBytesToRead;
	  LPDWORD      lpNumberOfBytesRead;
	  LPOVERLAPPED lpOverlapped;
  };
  using   SetFilePointerEx_Param = struct {
	  INT32         hFile;
	  INT64  liDistanceToMove;
	  PLARGE_INTEGER lpNewFilePointer;
	  DWORD          dwMoveMethod;
  };



  using _HANDLE = struct {
	INT32 seq;
	HANDLE handle;
  };

  using HandleHost = struct {
	  INT32 seq;
	  StringStorage host;
  };

  ~WebDav();

  void SendRequest(UINT reqCode, WebDavOperation* webdav, RfbOutputGate* output);
  void OnRequest(UINT repCode, DataInputStream* input, RfbOutputGate* output);
  void OnReply(DataInputStream* input);
  void OnDisconnect();
  void OnFail(DataInputStream* input);

protected:

  DWORD _CloseHandle(INT32 seq);
  DWORD _FindClose(INT32 seq);
  HANDLE GetHandle(INT32 seq);
  HANDLE GetFindHandle(INT32 seq);


  std::vector<WebDavOperation*> webdavTable;
  std::mutex  webdavMutex;

  std::mutex HandleMutex;
  std::vector<_HANDLE>  HandleTable;
  std::vector<_HANDLE>  FindHandleTable;

  public:
	  static void AddHandleHost(INT32 seq, LPCTSTR host);
	  static StringStorage RemoveHandleHost(INT32 seq);
	  static StringStorage GetHandleHost(INT32 seq);

	static std::mutex HandleMapMutex;
	static std::vector<HandleHost> HandleMap;
};

#endif
