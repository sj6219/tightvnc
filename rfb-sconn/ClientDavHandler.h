// Copyright (C) 2009,2010,2011,2012 GlavSoft LLC.
// All rights reserved.
//
//-------------------------------------------------------------------------
// This file is part of the TightVNC software.  Please visit our Web site:
//
//                       http://www.tightvnc.com/
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//-------------------------------------------------------------------------
//

#ifndef __CLIENT_DAV_HANDLER_H__
#define __CLIENT_DAV_HANDLER_H__

#include "RfbDispatcherListener.h"
#include "RfbCodeRegistrator.h"
#include "ClientInputEventListener.h"
#include "network/RfbOutputGate.h"
#include <vector>
#include <mutex>
#include <network/WebDavOperation.h>


class ClientDavHandler : public RfbDispatcherListener
{
public:
  ClientDavHandler(RfbCodeRegistrator* codeRegtor,
    ClientInputEventListener* extEventListener,
    RfbOutputGate* output,
    bool viewOnly);
  virtual ~ClientDavHandler();

  void setViewOnlyFlag(bool value) { m_viewOnly = value; }
  void PassMessage(const void* buffer, size_t len);

  bool m_isWebDavEnabled;

protected:
  // Listen function
  virtual void onRequest(UINT32 reqCode, RfbInputGate* input);

  ClientInputEventListener* m_extEventListener;
  RfbOutputGate* m_output;
  bool m_viewOnly;

  WebDav m_webdav;

};

#endif // __CLIENT_DAV_HANDLER_H__
