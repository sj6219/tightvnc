// Copyright (C) 2009,2010,2011,2012 GlavSoft LLC.
// All rights reserved.
//
//-------------------------------------------------------------------------
// This file is part of the TightVNC software.  Please visit our Web site:
//
//                       http://www.tightvnc.com/
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//-------------------------------------------------------------------------
//

#include "ClientDavHandler.h"
#include "rfb/MsgDefs.h"
#include "network/WebDavOperation.h"
#include "thread/AutoLock.h"
#include "network/TcpServer.h"
#include <vector>
#include <mutex>
#include <shared_mutex>
#include "io-lib/ByteArrayOutputStream.h"
#include "network\RfbOutputGate.h"
#include "io-lib/ByteArrayInputStream.h"
#include "rfb-sconn/RfbClient.h"
#include "rfb/MsgDefs.h"
#include "io-lib/ByteArrayOutputStream.h"
#include "network\RfbOutputGate.h"
#include "io-lib/ByteArrayInputStream.h"
#include "util/Clipboard.h"
#include "util/AnsiStringStorage.h"

#include "tvnserver-app/ControlServerOut.h"
#include "tvncontrol-app/ControlApplication.h"
#include "rfb/VendorDefs.h"



ClientDavHandler::ClientDavHandler(RfbCodeRegistrator* codeRegtor,
  ClientInputEventListener* extEventListener, RfbOutputGate *output,  bool viewOnly)
  : m_extEventListener(extEventListener),
  m_viewOnly(viewOnly),
    m_isWebDavEnabled(false),
  m_output(output)
{
  // Request codes
  codeRegtor->addClToSrvCap(ClientMsgDefs::ENABLE_WEB_DAV, VendorDefs::TIGHTVNC, WebDavDefs::WEB_DAV_SIG);

  codeRegtor->regCode(ClientMsgDefs::WEB_DAV_REQUEST, this);
  codeRegtor->regCode(ClientMsgDefs::WEB_DAV_REPLY, this);
  codeRegtor->regCode(ClientMsgDefs::ENABLE_WEB_DAV, this);
}

ClientDavHandler::~ClientDavHandler()
{
}

void ClientDavHandler::onRequest(UINT32 reqCode, RfbInputGate* input)
{
  switch (reqCode) {
    case ClientMsgDefs::WEB_DAV_REQUEST:
    {
      m_webdav.OnRequest(ServerMsgDefs::WEB_DAV_REPLY, input, m_output);
    }
    break;
    case ClientMsgDefs::WEB_DAV_REPLY:
    {
     // _RPT(L"ClientDavHandler::onRequest WEB_DAV_REPLY\n");

      UINT32 size = input->readUInt32();
      char* buffer = (char *) _malloca(size+1);
      buffer[0] = ClientMsgDefs::WEB_DAV_REPLY;
      input->readFully(buffer+1, size);

      ControlServerOut::WriteToPipe(buffer, size + 1);
      _freea(buffer);
    }
    break;
    case ClientMsgDefs::ENABLE_WEB_DAV:
    {
        m_isWebDavEnabled = true;
    }
    break;

  default:
    _ASSERT(0);
    StringStorage errMess;
    errMess.format(_T("Unknown %d protocol code received"), (int)reqCode);
    throw Exception(errMess.getString());
    break;
  }
}

void ClientDavHandler::PassMessage(const void *buffer, size_t len)
{
  AutoLock al(m_output);
  m_output->writeFully(buffer, len);
//  m_output->write(buffer, len)
  m_output->flush();
}

