// Copyright (C) 2011,2012 GlavSoft LLC.
// All rights reserved.
//
//-------------------------------------------------------------------------
// This file is part of the TightVNC software.  Please visit our Web site:
//
//                       http://www.tightvnc.com/
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//-------------------------------------------------------------------------
//

#include "util/AnsiStringStorage.h"
#include "util/Utf8StringStorage.h"


#include "RfbCutTextEventClientMessage.h"
#include "util/Clipboard.h"

#include "io-lib/ByteArrayOutputStream.h"
#define GETBYTE(x, n) (((x) >> ((n) * 8)) & 0xFF)


RfbCutTextEventClientMessage::RfbCutTextEventClientMessage(const StringStorage *cutText)
: m_cutText(*cutText)
{
}

RfbCutTextEventClientMessage::~RfbCutTextEventClientMessage()
{
}

void RfbCutTextEventClientMessage::send(RfbOutputGate *output)
{
	UINT format;
	StringStorage cutText = Clipboard::encodeString(m_cutText.getString(), &format);
	_RPT(_T("RfbCutTextEventClientMessage::send CLIENT_CUT_TEXT %d \"%s\"\n"), format, cutText.getString());
	AnsiStringStorage cutTextAnsi;
	cutTextAnsi.fromStringStorage(&cutText);
	UINT32 length = static_cast<UINT32>(cutTextAnsi.getLength());

	AutoLock al(output);
	output->writeUInt8(ClientMsgDefs::CLIENT_CUT_TEXT);
	output->writeUInt8(0); // padding 3 bytes
	output->writeUInt8(0);
	output->writeUInt8(0);
	output->writeUInt32(length+format);
	if (format)
	  output->writeUInt8(0);
	output->writeFully(cutTextAnsi.getString(), length);
	output->flush();
}

void RfbCutTextEventClientMessage::sendUtf8(RfbOutputGate *output)
{
  UINT format;
  StringStorage cutText = Clipboard::encodeString(m_cutText.getString(), &format);
  _RPT(_T("RfbCutTextEventClientMessage::send CLIENT_CUT_TEXT %d \"%s\"\n"), format, cutText.getString());
  Utf8StringStorage cutTextUtf;
  cutTextUtf.fromStringStorage(&cutText);
  UINT32 length = static_cast<UINT32>(cutTextUtf.getLength());

  AutoLock al(output);
  output->writeUInt32(ClientMsgDefs::CLIENT_CUT_TEXT_UTF8);
  output->writeUInt32(length+format);
  if (format)
	output->writeUInt8(0);
  output->writeFully(cutTextUtf.getString(), length);
  output->flush();
}
