A tightvnc program that implements a file clipboard using webdav

# Build

msbuild -t:clean tightvnc-dll.sln /p:Configuration=Release /p:Platform=x64
msbuild /v:m /m tightvnc-dll.sln /p:Configuration=Release /p:Platform=x64

cd webdav-server-rs
%comspec% /k "C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build\vcvarsx86_amd64.bat"
cargo rustc --release -- -C target-feature=+crt-static -C link-args=tvnviewer.res
cd ..

msbuild wix-installer\wix-installer.sln /p:Configuration=Release /p:Platform=x64

# Arm64 build

msbuild -t:clean tightvnc-dll.sln /p:Configuration=Release /p:Platform=arm64
msbuild /v:m /m tightvnc-dll.sln /p:Configuration=Release /p:Platform=arm64

cd webdav-server-rs
%comspec% /k "C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build\vcvarsx86_arm64.bat"
cargo rustc --release --target=aarch64-pc-windows-msvc --  -C target-feature=+crt-static -C link-args=tvnviewer.res
cd ..

# License

GPL v2

