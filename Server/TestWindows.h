#pragma once

#include "resource.h"

#include <crtdbg.h>
#ifdef _DEBUG
#ifdef _UNICODE
#define _RPT(...)  _RPT_BASE_W(_CRT_WARN, NULL, 0, NULL,  __VA_ARGS__)
#else
#define _RPT(...)   _RPT_BASE(_CRT_WARN, NULL, 0, NULL,  __VA_ARGS__)
#endif
#else
#define _RPT(...)
#endif

