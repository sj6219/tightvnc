// TestWindows.cpp : Defines the entry point for the application.
//

#include "framework.h"
#include "TestWindows.h"
#include "..\util\StringStorage.h"

#pragma comment(lib, "delayimp")
#pragma comment(lib, "server-dll.lib")
//#pragma comment(linker, "/DELAYLOAD:server-dll.dll")

extern "C" __declspec(dllexport) int WINAPI  Main(int port);


#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);

    {
      _RPT(_T("Server %s\n"), lpCmdLine);
      Str<const TCHAR> slice(lpCmdLine);
      slice.m_begin = slice.find_not(_T(" \t"));
      slice.m_end = slice.find(_T(" \t"));
      TCHAR q0[] = _T("-controlservice");
      TCHAR q1[] = _T("-controlapp");
      if  (!(slice.len() == sizeof(q0) / sizeof(TCHAR) - 1 &&_tcsnicmp(slice.begin(), q0, sizeof(q0)/sizeof(TCHAR)-1) == 0
          || slice.len() == sizeof(q1) / sizeof(TCHAR) - 1 && _tcsnicmp(slice.begin(), q1, sizeof(q1) / sizeof(TCHAR) - 1) == 0))
        return Main(0);
    }
    {
      STARTUPINFO si;

      PROCESS_INFORMATION pi;
      ZeroMemory(&si, sizeof(si));
      si.cb = sizeof(si);
      ZeroMemory(&pi, sizeof(pi));

      TCHAR path[_MAX_PATH];
      GetModuleFileName(NULL, path, _MAX_PATH);
      TCHAR* p = path;
      for (; ;) {
        auto q = _tcschr(p, '\\');
        if (q == 0)
          break;
        p = q + 1;
      }
      auto size = _sctprintf(_T("\"%.*s" "tvnviewer.exe\" -s %s"), (int)(p - path), path, lpCmdLine) + 1;
      TCHAR* r = (TCHAR *) alloca(size * sizeof(TCHAR));
      //_stprintf_s(r, size, _T("%.*s"), (int)(p - path), path);
      //SetCurrentDirectory(r);
      _stprintf_s(r, size, _T("\"%.*s" "tvnviewer.exe\" -s %s"), (int)(p - path), path, lpCmdLine);
      if (!CreateProcess(NULL, r, NULL, NULL, TRUE, 0, 0, NULL, &si, &pi))
      {
        return 1;
      }
      _RPT(_T("Server CreateProcess %d\n"), pi.dwProcessId);

      WaitForSingleObject(pi.hProcess, INFINITE);
      CloseHandle(pi.hProcess);
      CloseHandle(pi.hThread);
      return 0;
    }

}


